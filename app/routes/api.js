const express   = require('express');
const router    = express.Router();


// Index Page
router.get('/', function(req,res){
  res.send({version: '0.0.1','description': 'REST API backend for OpenAlerting'});
})


// All alerts
router.get('/alerts', function(req,res){
  res.send({type: 'GET'});
})


// add a new alert
router.post('/alerts', function(req,res){
  res.send({type:'POST'});
})


// update an alert
router.put('/alerts/:id', function(req, res){
  res.send({type: 'PUT'});
});



// delete an alert
router.delete('/alerts/:id', function(req, res){
  res.send({type: 'DELETE'});
});


module.exports = router;
