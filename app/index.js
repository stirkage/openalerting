const express       = require('express');
const bodyParser   = require('body-parser');


const port          = process.env.port || 4000;
const app           = express();

app.use(bodyParser.json());
app.use('/api',require('./routes/api'));

// Start Server
app.listen(process.env.port || 4000, function(){
  console.log("Server started on port: " + port)
});
